﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireProjectile : MonoBehaviour
{
    [Header("Bout du canon")]
    [Tooltip("Place le firepoint ici")]
    public List<Transform> firePointsList;

    [Header("Bullet Settings")]
    public GameObject bulletPrefab;
    public float bulletLifeTime = 3f;
    public bool fireSingle;

    Transform firePoint;
    int firePointTotalNumber;
    int firePointNumber;

    void Start()
    {
        firePointNumber = 0;
        firePointTotalNumber = firePointsList.Count;
        firePoint = firePointsList[firePointNumber];
    }

    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            if(!fireSingle)
            {
               foreach(Transform firePoint in firePointsList)
                {
                    GameObject projectile =  (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                    Destroy(projectile, bulletLifeTime);
                } 
            } 
            else
            {
                if(firePointNumber < firePointTotalNumber -1)
                {
                    firePointNumber ++;
                } else
                {
                    firePointNumber = 0;
                }

                firePoint = firePointsList[firePointNumber];
                GameObject projectile =  (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                Destroy(projectile, bulletLifeTime);

            }
            
        }
    }
}
