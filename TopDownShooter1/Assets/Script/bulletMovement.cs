﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletMovement : MonoBehaviour
{
    public float bulletSpeed = 10f;

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(0, 0, bulletSpeed * Time.deltaTime);
    }
}
