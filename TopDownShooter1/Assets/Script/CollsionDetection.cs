﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollsionDetection : MonoBehaviour
{
    [System.Serializable]
    public class CollisionEnterEnnemiEvent : UnityEvent<GameObject> { }
    public CollisionEnterEnnemiEvent collisionEnnemi;
    public UnityEvent collisionGeneral;

    private void OnCollisionEnter(Collision c)
    {
        if(c.gameObject.tag == "Ennemi")
        {
            collisionEnnemi.Invoke(c.gameObject);
        }
        collisionGeneral.Invoke();
    }
}
