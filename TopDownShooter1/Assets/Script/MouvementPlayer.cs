﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouvementPlayer : MonoBehaviour
{
public float moveSpeed = 10f;
public float gravityY = -1f;

    Rigidbody rb;
    CharacterController cc;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cc = GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        Vector3 dirMov = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        dirMov.y = gravityY;
        cc.Move(dirMov * moveSpeed * Time.deltaTime);
    }
}
