﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationBody : MonoBehaviour
{
    public LayerMask layer;
    public GameObject playerMesh;
    public float damping = 10f;
    Camera cam;

    void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        Ray r = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if(Physics.Raycast(r, out hitInfo, Mathf.Infinity, layer))
        {
            Vector3 hitPos = hitInfo.point;
            
            Vector3 dirOrientation = (hitPos - transform.position).normalized;
            dirOrientation.y = 0;

            Quaternion lookRotation = Quaternion.LookRotation(dirOrientation, Vector3.up);
            Quaternion smoothRotation = Quaternion.Slerp(playerMesh.transform.rotation, lookRotation, damping * Time.deltaTime);
            playerMesh.transform.rotation = smoothRotation;
        }
    }
}
