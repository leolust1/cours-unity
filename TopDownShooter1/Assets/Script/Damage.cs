﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public float damageBullet = -1;

    public void DoDamage(GameObject ennemi)
    {
        if(ennemi.tag == "Ennemi")
        {
            ennemi.GetComponent<Health>().ChangerPV(damageBullet);
        }
    }
}
