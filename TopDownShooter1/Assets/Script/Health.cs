﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float startLifePoint = 10f;

    public float currentLifePoint;

    Material mat;
    Color colorMat;

    private void Start()
    {
        mat = GetComponent<Renderer>().material;
        colorMat = mat.color;
        currentLifePoint = startLifePoint;
    }

    public void ChangerPV(float value)
    {
        currentLifePoint += value;

        StopCoroutine(ChangeColor());
        StartCoroutine(ChangeColor());
        
        if(currentLifePoint <= 0)
        {
            GetComponent<DestroyMyself>().DestroyMe();
        }
    }

    IEnumerator ChangeColor()
    {
        float t = 0f;
        float delay = 0.2f;

        while (t < delay)
        {
            Color colorLerp = Color.Lerp(colorMat, Color.red, t/delay);
            t += Time.deltaTime;
            mat.color = colorLerp;
            yield return new WaitForEndOfFrame();
        }

        while (t < delay)
        {
            Color colorLerp = Color.Lerp(Color.red, colorMat, t/delay);
            t += Time.deltaTime;
            mat.color = colorLerp;
            yield return new WaitForEndOfFrame();
        }
        mat.color = colorMat;

    }
}
